﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    public GameObject EnemyPrefab;
    public GameObject FoodPrefab;

    public float minX = -30.0f;
    public float maxX = 30.0f;

    public float minZ = -30.0f;
    public float maxZ = 30.0f;

    public int NumberofFoodAtRuntime = 30;
    public int NumberofEnemyAIAtRuntime = 5;

    public int FoodCap = 100;
    public int AICap = 10;

    public float EnemiesPerSecond = 1.0f;

    void Awake()
    {  
        for (int i = 0; i < NumberofFoodAtRuntime; i++)
        {
            Spawn(FoodPrefab);
        }

        for (int i = 0; i < NumberofEnemyAIAtRuntime; i++)
        {
            Spawn(EnemyPrefab);
        }
    }
	
    void Update()
    {
        float timer = Time.deltaTime;

        if (timer >= EnemiesPerSecond)
        {
            while (CheckNumberofFood() <= FoodCap)
            {
                Spawn(FoodPrefab);
            }

            while (CheckNumberofAI() <= AICap)
            {
                Spawn(EnemyPrefab);
            }
        }
    }

    void Spawn(GameObject Prefab)
    {
        Vector3 SpawnPoint = new Vector3(Random.Range(minX, maxX), 0.5f, Random.Range(minZ, maxZ));
        Instantiate(Prefab, SpawnPoint, Quaternion.identity);
    }

    int CheckNumberofFood()
    {
        GameObject[] gam = GameObject.FindGameObjectsWithTag("Food");
        return gam.Length;
    }

    int CheckNumberofAI()
    {
        GameObject[] gam = GameObject.FindGameObjectsWithTag("Enemy");
        return gam.Length;
    }

}
