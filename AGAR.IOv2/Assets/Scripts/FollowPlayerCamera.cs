﻿using UnityEngine;
using System.Collections;

public class FollowPlayerCamera : MonoBehaviour {

    public GameObject target;
    public float speed = 5.0f;

    private float Distance;

    private float Scale;

    private float CamOrthSize;
    
    void Start()
    {
        
        CamOrthSize = 10;
    }
     
	void Update ()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        Scale = target.GetComponent<PlayerEat>().Scale;   
        Camera.main.orthographicSize = Mathf.Lerp(CamOrthSize, Scale, Time.deltaTime * 5);

    }
}  
