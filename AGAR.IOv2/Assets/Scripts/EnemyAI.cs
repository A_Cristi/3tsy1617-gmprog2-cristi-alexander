﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

    private Vector3 Size = new Vector3(0.2f, 0.2f, 0.2f);

    public float Speed = 3.0f;   

    void Update()
    {
        //transform.position = Vector3.MoveTowards(transform.position, FindClosestEnemy("Player").transform.position, Speed * Time.deltaTime / transform.localScale.x);
        float DistanceToFood = Vector3.Distance(transform.position, FindClosestObj("Food").transform.position);
        float DistanceToPlayer = Vector3.Distance(transform.position, FindClosestObj("Player").transform.position);
        float DistanceToAI = Vector3.Distance(transform.position, FindClosestObj("Enemy").transform.position);
       
       if ((DistanceToAI < DistanceToPlayer || DistanceToAI < DistanceToFood)            
            && transform.localScale.x > FindClosestObj("Enemy").transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Enemy").transform.position, Speed * Time.deltaTime / transform.localScale.x);
            Debug.Log("Following AI");
        }

        else if ((DistanceToPlayer < DistanceToAI || DistanceToPlayer < DistanceToFood)
            && transform.localScale.x < FindClosestObj("Enemy").transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Player").transform.position, -Speed * Time.deltaTime / transform.localScale.x);
            Debug.Log("Running from Enemy");

        }
        else if ((DistanceToPlayer < DistanceToAI || DistanceToPlayer < DistanceToFood)
            && transform.localScale.x > FindClosestObj("Player").transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Player").transform.position, Speed * Time.deltaTime / transform.localScale.x);
            Debug.Log("Following Player");
        }

        else if ((DistanceToPlayer < DistanceToAI || DistanceToPlayer < DistanceToFood)
            && transform.localScale.x < FindClosestObj("Player").transform.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Player").transform.position, -Speed * Time.deltaTime / transform.localScale.x);
            Debug.Log("Running from Player");
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, FindClosestObj("Food").transform.position, Speed * Time.deltaTime / transform.localScale.x);
            Debug.Log("Following food");
        }
    }

    GameObject FindClosestObj(string Tag)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(Tag);
        GameObject closest = null;
        
        float distance = Mathf.Infinity;
        //float distance = 10.0f;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Food")
        {
            Destroy(other.gameObject);

            transform.localScale += Size;          
        }

    }
}
