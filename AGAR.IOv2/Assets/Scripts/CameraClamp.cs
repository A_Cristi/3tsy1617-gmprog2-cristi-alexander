﻿using UnityEngine;
using System.Collections;

public class CameraClamp : MonoBehaviour {
    
	void Update ()
    {
        Vector3 cameralock = transform.position;
        cameralock.y = Mathf.Clamp(transform.position.y, 15.0f, 15.1f);
        transform.position = cameralock;
    }
}
