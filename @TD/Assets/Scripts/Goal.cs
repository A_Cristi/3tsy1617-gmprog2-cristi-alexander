﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

    public Text HPText;

    public int HP = 10;
  
    void Update()
    {
        HPText.text = "HP - " + HP.ToString();
        if (HP <= 0)
        {
            //Application.Quit();
            SceneManager.LoadScene("Over");
        }
    }

    void OnCollisionEnter(Collision other)
    {
        Debug.Log("Collision Detected!");
        if (other.gameObject.tag == "Enemy")
        {
            Destroy(other.gameObject);
            HP -= 1;
        }

        if (other.gameObject.tag == "Boss")
        {
            HP = 0;
        }

    }
}
