﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour {

    public GameObject Flying, Ground, Boss;
    public int NumIfWaves;
    public int ItemsPerWave;

    public Text WaveCounter;

    public GameObject SpawnLoc;
    GameObject[] Monsters;
    int WaveNum;
    bool HasMonsters;

	// Use this for initialization
	void Start () {
        WaveNum = 1;
        HasMonsters = false;
	}

    // Update is called once per frame
    void Update()
    {
        CheckMonster();
        if (HasMonsters == false)
        {
            SpawnWave();
        }
        int WaveTemp = WaveNum - 1;
        

        WaveCounter.text = "Wave - " + WaveTemp.ToString();
        if (WaveNum == ItemsPerWave)
        {
            SceneManager.LoadScene("Win");

        }
    }

    void SpawnWave()
    {
        if (WaveNum % 2 == 0 && WaveNum % 5 != 0)
        {
            //Ground.GetComponent<EnemyHealth>().GoldMulti += WaveNum;
            //Ground.GetComponent<EnemyHealth>().HPMulti += (WaveNum * 100);
            for (int i = 0; i < ItemsPerWave; i++)
            {
                Instantiate(Ground, SpawnLoc.transform.position, Quaternion.identity);
            }           
        }

        if (WaveNum % 2 == 1 && WaveNum % 5 != 0)
        {
           //Flying.GetComponent<EnemyHealth>().GoldMulti += WaveNum;
            //Flying.GetComponent<EnemyHealth>().HPMulti += (WaveNum * 100);
            for (int i = 0; i < ItemsPerWave; i++)
            {
                Instantiate(Flying, SpawnLoc.transform.position, Quaternion.identity);
            }
        }

        if (WaveNum % 5 == 0)
        {
            //Boss.GetComponent<EnemyHealth>().GoldMulti += WaveNum;
            //Boss.GetComponent<EnemyHealth>().HPMulti += (WaveNum * 100);

            Instantiate(Boss, SpawnLoc.transform.position, Quaternion.identity);
        }
        Debug.Log("SpawnWave: " + WaveNum);
        WaveNum++;
    }

    void CheckMonster()
    {
        Monsters = GameObject.FindGameObjectsWithTag("Enemy");
        
        if (Monsters.Length <= 0)
        {
            HasMonsters = false;
            //Debug.Log("No monsters!");
        }

        else
        {
            HasMonsters = true;
            //Debug.Log("Monsters!");
        }

    }
}
