﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI    ;

public class ButtonIsClicked : MonoBehaviour {

    public Button yourButton;

    public bool IsClicked;

    // Use this for initialization
    void Start () {

        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        IsClicked = false;
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void TaskOnClick()
    {
        Debug.Log("You have clicked the button!");
        IsClicked = true;
    }

}
