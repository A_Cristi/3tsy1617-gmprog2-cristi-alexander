﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BType
{
    Normal, Cannon, Ice, Fire
}

public class Bullet : MonoBehaviour {
    private GameObject target;

    public float speed = 70f;

    public float Lowdamage;
    public float Highdamage;

    private float damage;

    public BType BulType;
    public float SlowAmt;
    public float SplashRad;
    public float FireDur;

    public void Seek(GameObject tgt)
    {
        target = tgt;
    }
    // Update is called once per frame

    private void Start()
    {
        AssignDam();
        CheckTowerLvl();
    }
    void Update () {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }       
        gotoTarget();
    } 

    void AssignDam()
    {
        damage = Random.Range(Lowdamage, Highdamage);
    }

    void gotoTarget()
    {        
        //Vector3 TargetPos = new Vector3(target.position.x, target.position.y, target.position.z);
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        
        if (transform.position == target.transform.position)
        {
            if (target != null)
            {
                target.GetComponent<EnemyHealth>().Health -= damage;
                //Debug.Log("Enemy HP is now" + target.GetComponent<EnemyHealth>().Health.ToString() );
                CheckEffects();
            }
            Destroy(gameObject);
        }
    }

    void CheckEffects()
    {
        switch (BulType)
        {
            case (BType.Ice):
                target.GetComponent<EnemyMovement>().SlowDown(SlowAmt);                
                break;

            case (BType.Cannon):
                Splash();
                break;

            case (BType.Fire):
                target.GetComponent<EnemyHealth>().Burn(FireDur, damage);
                break;

            case (BType.Normal):
                break;
        }

    }

    void Splash()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, SplashRad);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                collider.GetComponent<EnemyHealth>().Health -= damage;
            }
        }
        Debug.Log("SPLASH");
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, SplashRad);
    }

    public GameObject FindTower()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Tower");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    void CheckTowerLvl()
    {


        GameObject twr = FindTower();
        damage += twr.GetComponent<BaseTowerScript>().AdditionalDamage; 
        
        switch(twr.GetComponent<TowerType>().Ttype)
        {
            case TwrType.Arrow:
                break;

            case TwrType.Cannon:
                switch(twr.GetComponent<TowerType>().TowerLvl)
                {
                    case 0:
                        break;

                    case 1:
                        SplashRad += .5f;
                        break;

                    case 2:
                        SplashRad += 1.0f;
                        break;

                    case 3:
                        SplashRad += 1.5f;
                        break;
                }
                break;

            case TwrType.Ice:
                switch (twr.GetComponent<TowerType>().TowerLvl)
                {
                    case 0:
                        break;

                    case 1:
                        SplashRad += .5f;
                        SlowAmt += .5f;
                        break;

                    case 2:
                        SplashRad += 1.0f;
                        SlowAmt += 1.0f;
                        break;

                    case 3:
                        SplashRad += 1.5f;
                        SlowAmt += 1.5f;
                        break;
                }
                break;

            case TwrType.Fire:
                switch (twr.GetComponent<TowerType>().TowerLvl)
                {
                    case 0:
                        break;

                    case 1:
                        SplashRad += .5f;
                        FireDur += .1f;
                        break;

                    case 2:
                        SplashRad += 1.0f;
                        FireDur += .3f;
                        break;

                    case 3:
                        SplashRad += 1.5f;
                        FireDur += .5f;
                        break;
                }
                break;
        }
    }

}
