﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TwrType
{
    Arrow, Cannon, Ice, Fire
}

public class TowerType : MonoBehaviour {
    public TwrType Ttype;
    public int TowerCost;
    public int TowerCostToLvl;
    public int TowerLvl;

    public string Name;
    public Text TowerSelected;
    public Text TowerLvlText;
    public Text TowerCostToLvlText;
    public Button ToUpgrade;

    Gold gld;

    private void Start()
    {
        TowerSelected = GameObject.Find("TowerSel").GetComponent<Text>();
        ToUpgrade = GameObject.Find("Upgrade").GetComponent<Button>();
        TowerLvlText = GameObject.Find("TowerLvl").GetComponent<Text>();
        TowerCostToLvlText = GameObject.Find("TowerCosttolvl").GetComponent<Text>();
        gld = GameObject.Find("GoldHolder").GetComponent<Gold>();
        TowerLvl = 0;
    }

    void Update()
    {
        switch(Ttype)
        {
            case TwrType.Arrow:
                Name = "Arrow Tower";                   
                break;

            case TwrType.Cannon:
                Name = "Cannon Tower";
                break;

            case TwrType.Ice:
                Name = "Ice Tower";
                break;

            case TwrType.Fire:
                Name = "Fire Tower";
                break;
        }


        if (ToUpgrade.GetComponent<ButtonIsClicked>().IsClicked == true)
        {
            Debug.Log("Clicked");
            ToUpgrade.GetComponent<ButtonIsClicked>().IsClicked = false;
            Upgrade();
        }
    }

    private void OnMouseDown()
    {
        TowerSelected.text = Name;
        TowerLvlText.text = TowerLvl.ToString();
        TowerCostToLvlText.text = TowerCostToLvl.ToString();
    }

    void Upgrade()
    {
        if (gld.GoldAmt >= TowerCostToLvl)
        {

            if (TowerLvl >= 3)
            {
                TowerLvlText.text = "Max level!";
            }
            else
            {
                gld.GoldAmt -= TowerCostToLvl;
                TowerLvl++;
                TowerLvlText.text = TowerLvl.ToString();
                gameObject.GetComponent<BaseTowerScript>().LevelUp();
            }
        }
    }
}
