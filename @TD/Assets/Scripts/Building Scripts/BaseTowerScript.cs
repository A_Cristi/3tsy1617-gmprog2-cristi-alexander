﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseTowerScript : MonoBehaviour {

    public GameObject BulletPrefab;

    public float fireRate;
    private float rateController = 0f;

    public float TWRrange = 15f;
    private GameObject target;

    public float RotateSpeed = 10f;
    public float AdditionalDamage;

    bool CanTargetFlying;

    // Use this for initialization
    void Start() {
        InvokeRepeating("FindTarget", 0f, 0.5f);
        AdditionalDamage = 0;
    }

    void FindTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float shortestDis = Mathf.Infinity;
        GameObject nearestEnemy = null;
        
        foreach (GameObject enemy in enemies)
        {
            float DisToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (DisToEnemy < shortestDis)
            {
                shortestDis = DisToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDis <= TWRrange)
        {
            target = nearestEnemy;
        }
        else
        {
            target = null;
        }

        Debug.Log("Finding Target");    
    }

    // Update is called once per frame
    void Update() {        

        if (rateController <= 0f)
        {
            if (target != null)
            {
                transform.LookAt(target.transform.position);
                ShootBullet();
                rateController = .5f / fireRate;
            }
        }

        
        rateController -= Time.deltaTime;
    }

    void ShootBullet()
    {
        Debug.Log("Firing");
        GameObject FiredBullet = Instantiate(BulletPrefab, transform.position, transform.rotation) as GameObject;
        Bullet bulletF = FiredBullet.GetComponent<Bullet>();

        if (bulletF != null)
        {
            Debug.Log("TargetFound!");
            bulletF.Seek(target);
        }
    }

    public void LevelUp()
    {
        switch(gameObject.GetComponent<TowerType>().TowerLvl)
        {
            case 0:
                break;

            case 1:
                fireRate -= .3f;
                AdditionalDamage = 15.0f;
                break;

            case 2:
                fireRate -= .4f;
                AdditionalDamage = 20.0f;
                break;
            case 3:
                fireRate -= .5f;
                AdditionalDamage = 25.0f;
                break;

        }
    }

    GameObject[] Targets()
    {


        return Targets();
    }
}
