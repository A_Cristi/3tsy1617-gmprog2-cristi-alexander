﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemType {
    Normal,
    Flying,
    Boss

}

public class EnemyType : MonoBehaviour {
    public EnemType EnemMyType;
}
