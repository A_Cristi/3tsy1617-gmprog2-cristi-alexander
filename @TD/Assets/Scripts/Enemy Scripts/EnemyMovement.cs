﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {
    
    public GameObject end;

    public float Speed = 4.0f;
    float timeCounter = 0.0f;
    float TimetoSlow = 2.0f;

    UnityEngine.AI.NavMeshAgent nav;   

    // Use this for initialization
    void Start () {
        end = GameObject.Find("Goal");

        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
        nav.destination = end.transform.position;

        
    }
	
	// Update is called once per frame
	void Update () {

        nav.speed = Speed;

        CheckDes();
        timeCounter += Time.deltaTime;
    }

    void CheckDes()
    {
        
        if (gameObject.transform.position.z <= end.transform.position.z)
        {
            end.GetComponent<Goal>().HP--;
            Debug.Log("Died");
            Destroy(gameObject);
        }
    }

    public void SlowDown(float speed)
    {
        timeCounter = 0.0f;
        Debug.Log("Slowed!");
        if (timeCounter < TimetoSlow)
        {
            nav.speed = speed;            
        }

    }
}
