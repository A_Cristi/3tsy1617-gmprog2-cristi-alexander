﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;


public class EnemyHealth : MonoBehaviour {

    public float Health;
    public float HPMax;
    public int GoldMulti;
    public int HPMulti;

    private GameObject GoldH;
    private Gold GH;

    private Stopwatch DotTimer;
    // Use this for initialization
    void Start () { 
        GoldH = GameObject.Find("GoldHolder");
        GH = GoldH.GetComponent<Gold>();
        GoldMulti = 1;
        Health += HPMulti;
        DotTimer = new Stopwatch();
        HPMax = Health;      
	}
	
	// Update is called once per frame 
	void Update () {
        
        if (Health <= 0)
        {          

            switch (gameObject.GetComponent<EnemyType>().EnemMyType)
            {
                case EnemType.Boss:
                    GH.GoldAmt += 500 * GoldMulti;
                    break;

                case EnemType.Flying:
                    GH.GoldAmt += 200 * GoldMulti;
                    break;

                case EnemType.Normal:
                    GH.GoldAmt += 100 * GoldMulti;
                    break;
            }       
           
            Destroy(gameObject);
        }
    }

    public void Burn(float duration, float dmg)
    {
        //DotTimer.Start();

        //while (DotTimer.Elapsed.TotalSeconds <= duration)
        //{
        //    UnityEngine.Debug.Log("Burning!");
        //    DAM(dmg);
        //}
        //DotTimer.Stop();
        //DotTimer.Reset();
        float tempDura = duration;

        tempDura -= Time.deltaTime;
        if (tempDura <= 0.0f)
        {
            Health -= dmg;
            tempDura = duration;
            UnityEngine.Debug.Log("Burning!");
        }
    }

    //IEnumerator DAM(float dmg)
    //{
        //Health -= dmg;
        //UnityEngine.Debug.Log("B U R N");
        //yield return new WaitForSeconds(1);
    //}
}
