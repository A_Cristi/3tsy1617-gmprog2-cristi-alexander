﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
    
    public int speed = 5;

    public int Boundary = 40;

    private int ScreenWidth;
    private int ScreenHeight;

    // Use this for initialization
    void Start()
    {
        ScreenWidth = Screen.width;
        ScreenHeight = Screen.height;

    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += Vector3.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.back * speed * Time.deltaTime;
        }

        Vector3 CamClamp = transform.position;
        CamClamp.x = Mathf.Clamp(transform.position.x, 0, 63.0f);
        CamClamp.z = Mathf.Clamp(transform.position.z, 0, 63.0f);
        CamClamp.y = transform.position.y;

        transform.position = CamClamp;
        PanCam();

    }

    void PanCam()
    {
        Vector3 CamPos = transform.position;

        if (Input.mousePosition.x > ScreenWidth - Boundary)
        {
            CamPos.x += speed * Time.deltaTime;
        }

        if (Input.mousePosition.x < 0 + Boundary)
        {
            CamPos.x -= speed * Time.deltaTime;
        }

        if (Input.mousePosition.y > ScreenHeight - Boundary)
        {
            CamPos.z += speed * Time.deltaTime;
        }

        if (Input.mousePosition.y < 0 + Boundary)
        {
            CamPos.z -= speed * Time.deltaTime;
        }

        transform.position = CamPos;

    }
}
