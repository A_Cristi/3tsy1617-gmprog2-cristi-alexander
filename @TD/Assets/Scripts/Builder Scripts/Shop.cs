﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

    public GameObject ArrowTower;
    public GameObject CannonTower;
    public GameObject IceTower;
    public GameObject FireTower;

    public GameObject Selected;
    public Text TowerDisplay;
    public Text TowerCost;

        
    public Button Arrow, Cannon, Ice, Fire;
    private Button arr, cnn, ice, fire;

    // Use this for initialization
    void Start () {
        arr = Arrow.GetComponent<Button>();
        arr.onClick.AddListener(SelectArrow);

        cnn = Cannon.GetComponent<Button>();
        cnn.onClick.AddListener(SelectCannon);

        ice = Ice.GetComponent<Button>();
        ice.onClick.AddListener(SelectIce);

        fire = Fire.GetComponent<Button>();
        fire.onClick.AddListener(SelectFire);
    }   
	
	// Update is called once per frame
	void Update () {
        if (Selected != null)
            TowerCost.text = Selected.GetComponent<TowerType>().TowerCost.ToString();
    }

    public void SelectArrow()
    {
        Selected = ArrowTower;
        
        TowerDisplay.text = "Arrow Tower";
    }

    public void SelectCannon()
    {
        Selected = CannonTower;
        TowerDisplay.text = "Cannon Tower";
    }

    public void SelectIce()
    {
        Selected = IceTower;
        TowerDisplay.text = "Ice Tower";
    }

    public void SelectFire()
    {
        Selected = FireTower;
        TowerDisplay.text = "Fire Tower";
    }
}
