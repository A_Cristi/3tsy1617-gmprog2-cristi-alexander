﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base : MonoBehaviour {
 
    private bool HasBuilding;

    Gold gold;

    public GameObject GoldHolder;    

    private GameObject ShopHolder;
    private Shop SH;

    private float counterTime;
    public float BuildTime;

    public Text TowerSelected;

    private GameObject TowerOnMe;

    // Use this for initialization
    void Start () { 
        HasBuilding = false;
        gold = GoldHolder.GetComponent<Gold>();
        ShopHolder = GameObject.Find("ShopHolder");
        SH = ShopHolder.GetComponent<Shop>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseUpAsButton()
    {
        if (SH.Selected != null)
        {            
            if (HasBuilding == false && gold.GoldAmt >= SH.Selected.GetComponent<TowerType>().TowerCost)
            {
                HasBuilding = true;
                Debug.Log("Building" + SH.Selected.ToString());
                Invoke("Build", BuildTime);
            }
        }
    }

    void Build()
    {
        GameObject TowerOnMe = GameObject.Instantiate(SH.Selected, transform.position, Quaternion.identity);
        gold.GoldAmt -= SH.Selected.GetComponent<TowerType>().TowerCost;
        
    }
}
