﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Meteor : MonoBehaviour {

    public GameObject MeteorPrefab;

    public GameObject MeteButton;
    public Text CDMeteText;
    public float CD;

    private float CDMan = 0;
    private bool isCD = false;

    private GameObject camera;

    // Use this for initialization
    void Start () {
        camera = GameObject.Find("Camera");
	}

    // Update is called once per frame
    void Update()
    {

        if (MeteButton.GetComponent<ButtonIsClicked>().IsClicked == true)
        {
            if (isCD == false)
            {
                Activate();
                isCD = true;
                CDMan = CD;
            }
            else
            {
               
                if (CDMan <= 0)
                {
                    CDMan = 0.0f;
                    isCD = false;
                }

            }

            
        }
        CDMan -= Time.deltaTime;
        MeteButton.GetComponent<ButtonIsClicked>().IsClicked = false;
        //CDMeteText.text = CDMan.ToString();

        if (CDMan < 0.0f)
        {
            CDMeteText.text = "Available";

        }

        else
        {
            CDMeteText.text = CDMan.ToString();
        }

    }


    void Activate()
    {
        Debug.Log("Mete");
        Instantiate(MeteorPrefab, camera.transform.position, Quaternion.identity);

    }
}
