﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Reaper : MonoBehaviour {    
    public float Rate;
    public Text ReapCDtext;

    float HPpercentage;
    private GameObject[] Enemies;

    // Use this for initialization
    void Start () {
        
        InvokeRepeating("Search", 2.0f, Rate);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Search()
    {
        Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        List<GameObject> Below15 = new List<GameObject>();
       
        foreach (GameObject enem in Enemies)
        {
           HPpercentage = enem.GetComponent<EnemyHealth>().Health/ 
                enem.GetComponent<EnemyHealth>().HPMax; 

            if (HPpercentage <= .15f)
            {
                //Below15.Add(enem);
                Explode(enem, HPpercentage);
            }

        }
        //if (Below15 != null || Below15.Count > 0)
        //{
        //    Debug.Log("found!");
        //    int chosen = Random.Range(0, Below15.Count);
        //    Explode(Below15[chosen], HPpercentage);
        //}   
    }



    void Explode(GameObject enem, float dmg)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1.5f);
            
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                collider.GetComponent<EnemyHealth>().Health -= dmg * 100;
            }
        }
        Destroy(enem);
    }
}
