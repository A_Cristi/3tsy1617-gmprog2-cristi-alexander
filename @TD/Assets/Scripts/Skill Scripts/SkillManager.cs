﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillManager : MonoBehaviour {

    public GameObject Apocalypse;
    Button ApoButton;
    Image ApoImage;

    public GameObject Meteor;
    Button MeteButton;
    Image MeteImage;

    public GameObject Reaper;
    Image ReapImg;

    
    float ApoCD, MeteCD, ReapCD;
    float DisApoCD, DisMeteCD, DisReapCD;
    Text DisApoCDText, DisMeteCDText, DisReapCDText;

    // Use this for initialization
    void Start () {
        ApoButton = Apocalypse.GetComponent<Button>();
        ApoImage = Apocalypse.GetComponent<Image>();

        MeteButton = Meteor.GetComponent<Button>();
        ReapImg = Reaper.GetComponent<Image>();

        DisMeteCDText = GameObject.Find("CDMet").GetComponent<Text>();

        ApoCD = 60.0f;
        MeteCD = 20.0f;
        ReapCD = 10.0f;

        DisApoCD = ApoCD;
        DisMeteCD = MeteCD;
        DisReapCD = ReapCD;

	}
	
	// Update is called once per frame
	void Update () {

        ApoCD -= Time.time;
        MeteCD -= Time.time;
        ReapCD -= Time.time;

        if (Apocalypse.GetComponent<ButtonIsClicked>().IsClicked == true)
        {
            if (ApoCD > 0)
            {

                return;
            }
                
            else
            {
                SkillApo();

                Apocalypse.GetComponent<ButtonIsClicked>().IsClicked = false;
            }
            
        }

        if (Meteor.GetComponent<ButtonIsClicked>().IsClicked == true)
        {
            if (MeteCD > 0)
                return;
        }



	}

    void SkillApo()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject Enemy in enemies)
        {
           //create new array then assign
        }


        Debug.Log("Apocalypse Clicked");
        ApoCD = 60.0f;
    }

    void SkillMet()
    {
        

    }

   
}
