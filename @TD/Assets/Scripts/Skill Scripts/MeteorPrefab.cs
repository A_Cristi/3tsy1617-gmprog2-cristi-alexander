﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorPrefab : MonoBehaviour {

    float HPpercentage;
    Vector3 Des;
    private GameObject camera1;

    // Use this for initialization
    void Start () {
        camera1 = GameObject.Find("Point");

        //Vector3 Des = new Vector3(camera1.transform.position.x, -6.0f, camera1.transform.position.z);
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, camera1.transform.position, 10.0f * Time.deltaTime);        

        Collider[] colliders = Physics.OverlapSphere(transform.position, 3f);

        if (colliders.Length <= 0)
        {
            foreach (Collider collider in colliders)
            {

                float HPpercentage = .85f * collider.GetComponent<EnemyHealth>().Health;

                if (collider.tag == "Enemy")
                {
                    collider.GetComponent<EnemyHealth>().Health -= HPpercentage;
                }
            }
            Destroy(gameObject, 5);
            if (transform.position == camera1.transform.position)
            {
                Destroy(gameObject);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 3f);
    }
}
