﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Apocalypse : MonoBehaviour {

    public GameObject ApoButton;
    public Text CDApoText;
    public float CD;

    private float CDMan = 0;
    private bool isCD = false;
    private GameObject[] Enemies;
    //private List<GameObject> RegEnemies;

    void Start()
    {
       
        
    }
    

	// Update is called once per frame
	void Update () {

        Enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (ApoButton.GetComponent<ButtonIsClicked>().IsClicked == true)
        {
            if (isCD == false)
            {
                Activate();
                isCD = true;
                CDMan = CD;
            }
            else
            {
                
                if (CDMan <= 0)
                {
                    CDMan = 0.0f;
                    isCD = false;
                }

            }
        }
        CDMan -= Time.deltaTime;
        ApoButton.GetComponent<ButtonIsClicked>().IsClicked = false;

        if (CDMan < 0.0f)
        {
            CDApoText.text = "Available";

        }

        else
        {
            CDApoText.text = CDMan.ToString();
        }   
	}

    void Activate()
    {
        Debug.Log("Apocalypse");
        List<GameObject> RegEnemies = new List<GameObject>();

        foreach(GameObject enem in Enemies)
        {
            switch(enem.GetComponent<EnemyType>().EnemMyType)
            {
                case EnemType.Normal:
                    RegEnemies.Add(enem);
                    break;

                case EnemType.Flying:
                    RegEnemies.Add(enem);
                    break;

                case EnemType.Boss:
                    //
                    break;

            }
            
        }

        foreach(GameObject enem in RegEnemies)
        {
            Debug.Log("FOund enemy");
            Destroy(enem);
        }


    }
}
