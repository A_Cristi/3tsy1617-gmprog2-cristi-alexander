﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour {

    public int Score;
    public Text ScoreTxt;

	// Update is called once per frame
	void Update () {

        ScoreTxt.text = "Score " + Score;
		
	}
}
