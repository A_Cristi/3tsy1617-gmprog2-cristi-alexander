﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed;
        
    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();

        rb.velocity = transform.up * speed;        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Asteroid")
        {
            Destroy(gameObject);           
        }
    }
}
