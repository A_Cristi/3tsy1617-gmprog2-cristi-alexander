﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    public float MoveSpeed;
    public int Life = 3;

    public GameObject bullet;
    public Transform spawner;

    public float fireRate;
    private float nextFire;

    public Text LifeText;

	// Update is called once per frame
	void Update () {


        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        transform.position += new Vector3(x, y, 0) * MoveSpeed * Time.deltaTime;

        //LifeText.text = "Life: " + Life;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -10.3f, 10.3f),
                                         Mathf.Clamp(transform.position.y, -8.0f, 10.5f),
                                         -1.0f);

        if (Input.GetKey(KeyCode.Space) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(bullet, spawner.position, spawner.rotation);
        }

        if (Life <= 0)
        {
            //Application.Quit();
            //Application.LoadLevel("End");
            SceneManager.LoadScene("End");
        }

        LifeText.text = "Life " + Life.ToString();
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Asteroid")
        {
            Life--;
        }
    }
}
