﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {

    GameObject Player;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

        if (Player == null)
        {
            Debug.Log("None found");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Bullet")
        {

            Debug.Log("Hit");
            Player.GetComponent<ScoreCounter>().Score += 1;
            Destroy(other.gameObject);            
        }
        Destroy(gameObject);
    }
}
