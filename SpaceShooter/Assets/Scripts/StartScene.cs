﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScene : MonoBehaviour {

    void Update()
    {
        if (Input.anyKey)
        {
            SceneManager.LoadScene("Main");
            Debug.Log("A key or mouse click has been detected");
        }

    }
}
